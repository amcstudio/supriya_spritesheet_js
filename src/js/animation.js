~ function () {
	'use strict';
	var $ = TweenMax,
		ad = document.getElementById('mainContent'),
		playspriteSheet;



	window.init = function () {
		var tl = new TimelineLite();

		spritesheetAnimation(200, 200, 12000)
		tl.to(['.bgCircle','#logo', '#endCopy'] , 0.5,{ opacity: 1, ease:Power1.easeInOut },'+=3')
	}

	function stopAnimate() {
		clearInterval(playspriteSheet);
	  } 

	  
	  function spritesheetAnimation(framePosition, frameWidth, imgWidth) {
		playspriteSheet = setInterval(function(){  
				document.getElementById("spriteSheet").style.backgroundPositionX = -1 * framePosition+'px';
			
				if (framePosition < imgWidth) {
					
						framePosition = framePosition + frameWidth;
						
				}
				else {
				stopAnimate();
				}		 		  
			}, 50); 
	  } 

}();

